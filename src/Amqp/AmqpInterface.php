<?php
/**
 * @author Alexander Stepanenko <alex.stepanenko@gmail.com>
 */

namespace Sdk\Events\Amqp;

interface AmqpInterface
{
    /**
     * Pushes message to immediate queue
     *
     * @param array $data
     */
    public function pushToQueue(array $data) : void;

    /**
     * Pushes message to delayed queue
     *
     * @param array $data
     * @param int $delay delay in seconds
     */
    public function pushToDelayedQueue(array $data, int $delay) : void;
}
